package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"time"
)

func main() {
	filename := os.Args[1]
	re := regexp.MustCompile(`created: *([0-9]+)`)
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		if re.MatchString(line) {
			//bline := []byte(line)
			parts := re.FindStringSubmatch(line)
			// fmt.Println(len(parts))
			// for i := range parts {
			dateInt, _ := strconv.ParseInt(parts[1], 10, 0)
			dateTime := time.Unix(dateInt, 0)

			fmt.Println("date: " + dateTime.Format(time.RFC3339))
			// }

		} else {
			fmt.Println(line)
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
