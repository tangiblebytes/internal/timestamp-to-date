# Timstamp to Date

I wrote this to convert some blog files

They were written in markdown with frontmatter

and had a line in the format

`created: 12234334`

Where the number was a unix timestamp

I needed this to be

`date: 2021-02-11T15:46:22Z`

run like

`stampDate $filename`

to receive the file with this line transformed - on standard out
